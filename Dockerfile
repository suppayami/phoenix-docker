FROM elixir:1.8-slim

RUN mix local.hex --force \
    && mix local.rebar --force \
    && mix archive.install --force hex phx_new 1.4.1 \
    && apt-get update \
    && curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt-get install -y libssl1.1 \
    && apt-get install -y erlang-crypto \
    && apt-get install -y nodejs \
    && apt-get install -y apt-utils \
    && apt-get install -y build-essential \
    && apt-get install -y inotify-tools \
    && apt-get install -y gettext-base \
    && apt-get install -y openssh-client